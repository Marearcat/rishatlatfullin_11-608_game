﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ArcanoidOOP
{
    class Ball
    {
        /// <summary>
        /// Создание шариков и бриков
        /// Смена вектора мячика во время столкновения и сдвиг, если мяч ни с чем не сталкивается
        /// Регенерация бриков при их отсутствии
        /// Подсчёт очков
        /// </summary>
        public void HPbar()
        {
            for (int i = 1; i < 8; i++)
            {
                Console.SetCursorPosition(b + i, 0);
                Console.Write('-');
            }
            Console.SetCursorPosition(b + 8, 0);
            Console.Write('+');
            for (int i = 1; i < 8; i++)
            {
                Console.SetCursorPosition(b + i, 8);
                Console.Write('-');
            }
            for (int i = 1; i < 8; i++)
            {
                Console.SetCursorPosition(b + 8, i);
                Console.Write('|');
            }
            Console.SetCursorPosition(b + 8, 8);
            Console.Write('+');
            Console.SetCursorPosition(b + 2, 2);
            Console.Write("HP=3");
            Console.SetCursorPosition(b + 2, 5);
            Console.Write("Score:");

        }
        public byte Health = 3;
        protected void minusHP()
        {
            Health--;
            Console.SetCursorPosition(b + 5, 2);
            Console.Write(Health);
        }
        public int points = 0;
        protected void IncreasePoints()
        {
            points++;
            Console.SetCursorPosition(b + 2, 6);
            Console.Write(points);
        }
        public bool beat;
        protected int[] board = new int[8];
        protected int hight;
        public void Check(int[] n, int m)
        {
            for (int i = 0; i < 8; i++)
                board[i] = n[i];
            hight = m;
        }
        public byte hard = 2;
        public int time;
        public int a;
        public int b;
        protected bool[,] brick;
        protected void Break(int i, int j)
        {
            brick[i, j] = false;
            Console.SetCursorPosition(i, j);
            Console.Write(' ');
            score--;
            IncreasePoints();
        }
        public int score;
        public void Build()
        {
            score = 0;
            brick = new bool[b + 2, a + 2];
            for (int i = 0; i <= b + 1; i++)
                for (int j = 0; j <= a + 1; j++)
                    brick[i, j] = false;
            Random rnd = new Random();
            for (int i = 1; i < b; i++)
                for (int j = 1; j < a - 10; j++)
                    if (rnd.Next(1, 8) > 8 - hard)
                    {
                        brick[i, j] = true;
                        Console.SetCursorPosition(i, j);
                        Console.Write('#');
                        score++;
                    }
        }
        public bool activ = false;
        public bool stat = true;
        public int x;
        public int y;
        public int prevx;
        public int prevy;
        public void spawn()
        {
            Console.SetCursorPosition(x, y);
            Console.Write('@');
            prevx = x - 1;
            prevy = y + 1;
        }
        public void Right()
        {
            if (x < b - 5)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(' ');
            }
        }
        public void Left()
        {
            if (x > 4)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(' ');
            }
        }
        public void Up()
        {
            if (y > a - 5)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(' ');
            }
        }
        public void Down()
        {
            if (y < a - 2)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(' ');
            }
        }
        public void Fly()
        {
            Thread Run = new Thread(Go);
            Run.Start();
        }
        protected void Go()
        {
            while (activ)
            {
                if (score == 0)
                    Build();
                if (beat)
                {
                    Console.SetCursorPosition(x, y);
                    Console.Write('@');
                }
                if ((prevx != 0) && (prevx != b) && (prevy != 0) && !Artifact(prevx, prevy) && beat && !brick[prevx, prevy])
                {
                    Console.SetCursorPosition(prevx, prevy);
                    Console.Write(' ');
                }
                if((y == a) && beat)
                {
                    activ = false;
                    stat = true;
                    minusHP();
                    x = board[3];
                    y = hight - 1;
                    spawn();
                    continue;
                }
                //up
                if((y < prevy) && beat)
                {
                    //right
                    if(x > prevx)
                    {
                        //conor
                        if (((y == 1) && (x == b - 1)) || (brick[x + 1, y] && brick[x, y - 1] && brick[x + 1, y - 1]))
                        {
                            URC();
                            continue;
                        }
                        //up wall
                        if ((y == 1) || brick[x, y - 1])
                        {
                            RbU();
                            continue;
                        }
                        //right wall
                        if ((x == b - 1) || brick[x + 1, y])
                        {
                            UbR();
                            continue;
                        }
                        //front bash
                        if (brick[x + 1, y - 1])
                        {
                            URReverse();
                            continue;
                        }
                        //just go
                        UR();
                    }
                    else
                    {
                        //conor
                        if (((y == 1) && (x == 1)) || (brick[x - 1, y] && brick[x, y - 1] && brick[x - 1, y - 1]))
                        {
                            ULC();
                            continue;
                        }
                        //up wall
                        if ((y == 1) || brick[x, y - 1])
                        {
                            LbU();
                            continue;
                        }
                        //left wall
                        if ((x == 1) || brick[x - 1, y])
                        {
                            UbL();
                            continue;
                        }
                        //front bash
                        if (brick[x - 1, y - 1])
                        {
                            ULReverse();
                            continue;
                        }
                        //just go
                        UL();
                    }
                }
                //down
                if ((y > prevy) && beat)
                {
                    //right
                    if (x > prevx)
                    {
                        //conor
                        if ((brick[x, y + 1] && ((brick[x + 1, y] && brick[x + 1, y + 1]) || (x == b - 1))) || ((x == b - 1) && UpArtifact(x, y)))
                        {
                            DRC();
                            continue;
                        }
                        //down wall
                        if (brick[x, y + 1] || UpArtifact(x, y))
                        {
                            RbD();
                            continue;
                        }
                        //right wall
                        if ((x == b - 1) || brick[x + 1, y] || ((x == board[0] - 1) && (y == hight)))
                        {
                            DbR();
                            continue;
                        }
                        //front bash
                        if (brick[x + 1, y + 1] || ((x + 1 == board[0]) && (y + 1 == hight)))
                        {
                            DRReverse();
                            continue;
                        }
                        //just go
                        DR();
                    }
                    else
                    {
                        //conor
                        if ((brick[x, y + 1] && ((brick[x - 1, y] && brick[x - 1, y + 1]) || (x == 1))) || ((x == 1) && UpArtifact(x, y)))
                        {
                            DLC();
                            continue;
                        }
                        //down wall
                        if (brick[x, y - 1] || UpArtifact(x, y))
                        {
                            LbD();
                            continue;
                        }
                        //left wall
                        if ((x == 1) || brick[x - 1, y] || ((x == board[7] + 1) && (y == hight)))
                        {
                            DbL();
                            continue;
                        }
                        //front bash
                        if (brick[x - 1, y + 1] || ((x - 1 == board[7]) && (y + 1 == hight)))
                        {
                            DLReverse();
                            continue;
                        }
                        //just go
                        DL();
                    }
                }
            }
        }
        //up-right methods
        protected void URC()
        {
            if (brick[x + 1, y])
                Break(x + 1, y);
            if (brick[x, y - 1])
                Break(x, y - 1);
            if (brick[x + 1, y - 1])
                Break(x + 1, y - 1);
            prevy = y - 1;
            prevx = x + 1;
        }
        protected void RbU()
        {
            if (brick[x, y - 1])
                Break(x, y - 1);
            prevy = y - 1;
        }
        protected void UbR()
        {
            if (brick[x + 1, y])
                Break(x + 1, y);
            prevx = x + 1;
        }
        protected void URReverse()
        {
            Break(x + 1, y - 1);
            prevx = x + 1;
            prevy = y - 1;
        }
        protected void UR()
        {
            x++;
            prevx++;
            y--;
            prevy--;
            Thread.Sleep(time);
        }
        //up-left methods
        protected void ULC()
        {
            if (brick[x - 1, y])
                Break(x - 1, y);
            if (brick[x, y - 1])
                Break(x, y - 1);
            if (brick[x - 1, y - 1])
                Break(x - 1, y - 1);
            prevy = y - 1;
            prevx = x - 1;
        }
        protected void LbU()
        {
            if (brick[x, y - 1])
                Break(x, y - 1);
            prevy = y - 1;
        }
        protected void UbL()
        {
            if (brick[x - 1, y])
                Break(x - 1, y);
            prevx = x - 1;
        }
        protected void ULReverse()
        {
            Break(x - 1, y - 1);
            prevx = x - 1;
            prevy = y - 1;
        }
        protected void UL()
        {
            x--;
            prevx--;
            y--;
            prevy--;
            Thread.Sleep(time);
        }
        //down-right methods
        protected void DRC()
        {
            if (brick[x + 1, y])
                Break(x + 1, y);
            if (brick[x, y + 1])
                Break(x, y + 1);
            if (brick[x + 1, y + 1])
                Break(x + 1, y + 1);
            prevy = y + 1;
            prevx = x + 1;
        }
        protected void RbD()
        {
            if (brick[x, y + 1])
                Break(x, y + 1);
            prevy = y + 1;
        }
        protected void DbR()
        {
            if (brick[x + 1, y])
                Break(x + 1, y);
            prevx = x + 1;
        }
        protected void DRReverse()
        {
            Break(x + 1, y + 1);
            prevx = x + 1;
            prevy = y + 1;
        }
        protected void DR()
        {
            x++;
            prevx++;
            y++;
            prevy++;
            Thread.Sleep(time);
        }
        //down-left methods
        protected void DLC()
        {
            if (brick[x - 1, y])
                Break(x - 1, y);
            if (brick[x, y + 1])
                Break(x, y + 1);
            if (brick[x - 1, y + 1])
                Break(x - 1, y + 1);
            prevy = y + 1;
            prevx = x - 1;
        }
        protected void LbD()
        {
            if (brick[x, y + 1])
                Break(x, y + 1);
            prevy = y + 1;
        }
        protected void DbL()
        {
            if (brick[x - 1, y])
                Break(x - 1, y);
            prevx = x - 1;
        }
        protected void DLReverse()
        {
            Break(x - 1, y + 1);
            prevx = x - 1;
            prevy = y + 1;
        }
        protected void DL()
        {
            x--;
            prevx--;
            y++;
            prevy++;
            Thread.Sleep(time);
        }
        //Artifacts
        protected bool UpArtifact(int f, int e)
        {
            bool arg = false;
            bool ord = false;
            for (int i = 0; i < 8; i++)
                if (f == board[i])
                    arg = true;
            if (e == hight - 1)
                ord = true;
            return arg && ord;
        }
        protected bool Artifact(int f, int e)
        {
            bool arg = false;
            bool ord = false;
            for (int i = 0; i < 8; i++)
                if (f == board[i])
                    arg = true;
            if (e == hight)
                ord = true;
            return arg && ord;
        }
    }
}
