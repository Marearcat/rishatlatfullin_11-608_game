﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcanoidOOP
{
    class Game
    {
        /// <summary>
        /// Создание экземпляров классов
        /// Присваивание необходимых значений
        /// Взаимодействие объектов
        //З/ </summary>
        static void Main(string[] args)
        {
            Cinema kino = new Cinema();
            kino.Intro();
            Terrain desk = new Terrain();
            Console.Clear();
            Console.WriteLine("Введите ширину доски 9 < x < 62");
            int alfa = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите высоту 8 < y < 400");
            int beta = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите сложность от 2 до 8");
            byte hardolis = Convert.ToByte(Console.ReadLine());
            desk.Draw(alfa, beta);
            Ball op = new Ball();
            op.hard = hardolis; //from 2 to 8
            Board player = new Board();
            player.a = desk.y;
            player.b = desk.x;
            player.Spawn();
            op.x = player.x[3];
            op.y = player.y - 1;
            op.a = desk.y;
            op.b = desk.x;
            op.HPbar();
            op.spawn();
            op.Build();
            op.time = 200;
            while (player.alive)
            {
                if(op.Health == 0)
                {
                    player.alive = false;
                    continue;
                }
                if (op.stat)
                {
                    op.x = player.x[3];
                    op.y = player.y - 1;
                    op.prevx = op.x - 1;
                    op.prevy = op.y + 1;
                    Console.SetCursorPosition(op.x, op.y);
                    Console.Write('@');
                }
                else
                {
                    op.Check(player.x, player.y);
                }
                if (player.alive)
                {
                    ConsoleKey key = Console.ReadKey(true).Key;
                    op.beat = false;
                    if (key == ConsoleKey.RightArrow)
                    {
                        if (op.stat)
                            op.Right();
                        player.Right();
                        op.beat = true;
                        continue;
                    }
                    if (key == ConsoleKey.LeftArrow)
                    {
                        if (op.stat)
                            op.Left();
                        player.Left();
                        op.beat = true;
                        continue;
                    }
                    if (key == ConsoleKey.UpArrow)
                    {
                        if (op.stat)
                            op.Up();
                        player.Up();
                        op.beat = true;
                        continue;
                    }
                    if (key == ConsoleKey.DownArrow)
                    {
                        player.Down();
                        if (op.stat)
                            op.Down();
                        op.beat = true;
                        continue;
                    }
                    if (key == ConsoleKey.Escape)
                    {
                        player.alive = false;
                        op.activ = false;
                    }
                    if ((key == ConsoleKey.Spacebar) && op.stat)
                    {
                        op.stat = false;
                        op.activ = true;
                        op.Fly();
                    }
                    op.beat = true;
                }
            }
            kino.score = op.points;
            kino.End();
        }
    }
}
