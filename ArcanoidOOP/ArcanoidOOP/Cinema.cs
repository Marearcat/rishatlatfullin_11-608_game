﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ArcanoidOOP
{
    class Cinema
    {
        /// <summary>
        /// Правила в начале игры
        /// Вывод очков в конце игры
        /// </summary>
        public int score = 0;
        public void Intro()
        {
            Console.SetCursorPosition(15, 5);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("RULES:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(5, 6);
            Console.WriteLine("Don't drop down the ball!!!!11!1");
            Console.SetCursorPosition(5, 7);
            Console.WriteLine("Move board by Arrows.");
            Console.SetCursorPosition(5, 8);
            Console.WriteLine("Fire with ball by space.");
            Thread.Sleep(5000);
            Console.Clear();
        }       
        public void End()
        {
            Console.Clear();
            Console.SetCursorPosition(15, 5);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("GAME OVER");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(15, 6);
            Console.Write("Score = ");
            Console.Write(score);
            Thread.Sleep(5000);
        }
    }
}
