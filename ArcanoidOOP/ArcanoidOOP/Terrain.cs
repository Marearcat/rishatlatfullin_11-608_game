﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcanoidOOP
{
    class Terrain
    {
        /// <summary>
        /// Создание рабочей области
        /// Отрисовка границ
        /// </summary>
        public int x;
        public int y;
        public void Draw(int x, int y)
        {
            Console.Clear();
            this.x = x;
            this.y = y;
            for (int i = 1; i < x; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write('-');
            }
            for (int i = 1; i < y; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write('|');
                Console.SetCursorPosition(x, i);
                Console.Write('|');
            }
            Console.SetCursorPosition(0, 0);
            Console.Write('+');
            Console.SetCursorPosition(x, 0);
            Console.Write('+');
            Console.ForegroundColor = ConsoleColor.Red;
            for(int i = 0; i <= x; i++)
            {
                Console.SetCursorPosition(i, y);
                Console.Write('x');
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
