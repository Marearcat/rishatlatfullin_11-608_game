﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ArcanoidOOP
{
    class Board
    {
        /// <summary>
        /// Создание и движение доски
        /// </summary>
        public int a;
        public int b;
        public bool alive = true;
        public int y;
        public int[] x = new int[8];
        public void Spawn()
        {
            y = a - 3;
            for (int i = 0; i < 8; i++)
            {
                x[i] = b / 2 - 3 + i;
                Console.SetCursorPosition(x[i], y);
                Console.Write('=');
            }
        }
        public void Right()
        {
            if (x[7] < b - 1)
            {
                Console.SetCursorPosition(x[0], y);
                Console.Write(' ');
                for (int i = 0; i < 8; i++)
                {
                    x[i]++;
                }
                Console.SetCursorPosition(x[7], y);
                Console.Write('=');
            }
        }
        public void Left()
        {
            if (x[0] > 1)
            {
                Console.SetCursorPosition(x[7], y);
                Console.Write(' ');
                for (int i = 0; i < 8; i++)
                {
                    x[i]--;
                }
                Console.SetCursorPosition(x[0], y);
                Console.Write('=');
            }
        }
        public void Down()
        {
            if(y < a - 1)
            {
                for(int i = 0;  i < 8; i++)
                {
                    Console.SetCursorPosition(x[i], y);
                    Console.Write(' ');
                }
                y++;
                for (int i = 0; i < 8; i++)
                {
                    Console.SetCursorPosition(x[i], y);
                    Console.Write('=');
                }
            }
        }
        public void Up()
        {
            if (y > a - 4)
            {
                for (int i = 0; i < 8; i++)
                {
                    Console.SetCursorPosition(x[i], y);
                    Console.Write(' ');
                }
                y--;
                for (int i = 0; i < 8; i++)
                {
                    Console.SetCursorPosition(x[i], y);
                    Console.Write('=');
                }
            }
        }
    }
}
